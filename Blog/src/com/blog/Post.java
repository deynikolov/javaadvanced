package com.blog;

public class Post {
	
	private static int serialNumber = 0;
	private int id = 0;
	private String title;
	private String content;

	public Post(String title, String content) {
		this.title = title;
		this.content = content;
		serialNumber++;
		id = serialNumber;
	}
	
	public static int getSerialNumber() {
		return serialNumber;
	}
	
	public String getTitle() {
		return title;
	}

	public boolean stringOccurInTitle(String substring) {
		return title.toLowerCase().contains(substring);
	}
	
	public boolean stringOccurInContent(String substring) {
		return content.toLowerCase().contains(substring);
	}
	
	public int substringOccurence (String substring) {
		int counter = 0;
		while(content.toLowerCase().contains(substring)) {counter++;}
		return counter;
	}

	@Override
	public String toString() {
		return 	"Post ID: " + id + "\n"  +
				"Post title: " + title + "\n" + 
				"Post content: " + content;
	}
	
}
