package com.blog;

import java.util.*;

public class Blog {
	
	private List<Post> posts;

	public Blog() {
		posts = new ArrayList<Post>();
	}
	
	public void addPost(Post post) {posts.add(post);}
	
	public List<Post> searchSubstringInPost(String substring) {
		List<Post> sortedBySubstringOccurence = new ArrayList<Post>();
		
		for(Post post: posts) {
			if(post.stringOccurInContent(substring.toLowerCase())) {sortedBySubstringOccurence.add(post);}
		}
		Post temporaryPost;
		int right = sortedBySubstringOccurence.size() - 1, counter;
		while(right > 0) {
			counter = 0;
			for(int i = 0; i < right ; i++) {
				if(sortedBySubstringOccurence.get(i).substringOccurence(substring) > 
				sortedBySubstringOccurence.get(i + 1).substringOccurence(substring)) {
					temporaryPost = sortedBySubstringOccurence.get(i);
					sortedBySubstringOccurence.set(i, sortedBySubstringOccurence.get(i + 1));
					sortedBySubstringOccurence.set(i + 1, temporaryPost);
					counter = i;
				}
			}
			right = counter;
		}
		return sortedBySubstringOccurence;
	}
	
	public void printResults(List<Post> posts) {
		for(Post post: posts) {System.out.println(post.toString() + "\n");}
	}

}
