package com.blog;

public class Main {
	public static void main(String[] args) {
		//https://stackify.com/oops-concepts-in-java/
		Post firstPost = new Post("Definition of OOP Concepts in Java",
				"concepts in Java are the main ideas behind Java�s Object Oriented Programming." + "\n" +
				"They are an abstraction, encapsulation, inheritance, and polymorphism."  + "\n" +
				"Grasping them is key to understanding how Java works."  + "\n" +
				"Basically, Java OOP concepts let us create working methods and variables,"  + "\n" +
				"then reuse all or part of them without compromising security.");
		//https://www.nextacademy.com/blog/object-oriented-programming-oop/
		Post secondPost = new Post("What is object oriented programming (OOP)?", 
				"Object Oriented Programming (OOP) is the foundation of mastering most of the widely"  + "\n" +
				"used programming languages such as Java, Python, Ruby and, of course, Swift."  + "\n" +
				"It helps you in the process of designing your app because an app without any proper"  + "\n" +
				"planning will make you go crazy (trust me, been there, done that).");
	
		Blog blog = new Blog();
		blog.addPost(firstPost);
		blog.addPost(secondPost);
		blog.printResults(blog.searchSubstringInPost("OOP"));
	}
}
