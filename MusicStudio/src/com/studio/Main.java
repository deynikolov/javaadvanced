package com.studio;

public class Main {	
	public static void main(String[] args) {
		Director dir1 = new Director("Ivan", 40);
		Director dir2 = new Director("Stoqn", 38);
		Studio st1 = new Studio(17, 8);
		Studio st2 = new Studio(19, 6);
		MusicCompany musicCompany = new MusicCompany(dir1, st1);
		MusicCompany musicCompany2 = new MusicCompany(dir2, st2);
		Client cl1 = new Client("Borislav", 26);
		Client cl2 = new Client("Ivan", 18);
		
		musicCompany.checkPrices(st2);
		musicCompany.rentStudio(cl1, 6);
		musicCompany2.rentStudio(cl2, 8);
		System.out.println(musicCompany.checkPrices(st2));
		System.out.println("Price in USD for renting studio1 is " + CurrencyCalc.calc(st1, "USD"));
		
		
	}
}
