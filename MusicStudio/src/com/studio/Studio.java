package com.studio;

public class Studio {
	private static int MinPrice = MarketPrice.getMarketPrice();
	private int price;
	private int total;
	private int workingHours;
	
	
	Studio(int price, int workingHours){
		if(price < MinPrice) {
			System.out.println("The price you are trying to create is below the market price!");}
		else {
			MarketPrice.setMarketPrices(price);
			this.price = price;
			this.workingHours = workingHours;
			}
	}


	public int getWorkingHours() {
		return workingHours;
	}


	public void setWorkingHours(int workingHours) {
			if(this.workingHours < 0) {
				System.out.println("You exceed the working hours of the studio.");
			}else {
				this.workingHours = workingHours;
			}
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int price) {
		this.total = this.total + price;
	}

	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}
	
	public void changePrice(int percentage) {
		this.setPrice(this.getPrice() * percentage);
	}
	
	@Override
	public String toString() {
		return 	"Price: " + this.price + "\n" +
				"Working hours " + this.workingHours;
	}
}
