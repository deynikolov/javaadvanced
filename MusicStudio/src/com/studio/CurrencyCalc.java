package com.studio;

public class CurrencyCalc {
	
	public static double calc(Studio st, String currency) {
		double total = 0;
		if(currency.equals("USD")) {
			total = st.getPrice() * 1.66;
		}else if(currency.equals("EUR")){
			total = st.getPrice() * 1.94;
		}
		return total;
	}
}
