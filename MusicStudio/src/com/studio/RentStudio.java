package com.studio;

public class RentStudio {
	private Director dir1, dir2;
	private Client cl1;
	private CurrencyCalc cc;
	
	RentStudio(){
		dir1 = new Director("Ivan", 40);
		dir2 = new Director("Stoqn", 38);
		cl1 = new Client("Borislav", 26);
		cc = new CurrencyCalc(dir1.getStudio());
	}
	
	public void rent() {
		dir1.setStudio(30, 8);
		dir2.setStudio(27, 6);
		cl1.rentStudio(dir1.getStudio(), 6);
		cc.calc("USD");
		System.out.println(cl1.checkPrices(dir1.getStudio(), dir2.getStudio()));
	}
}
