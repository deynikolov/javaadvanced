package com.studio;

public class Director extends Person{
	
	//private Studio st1;
	
	Director(String name, int age) {
		super(name, age);
//		st1 = new Studio(1, 1, 1);
	}
	
	public void compareStudioByDirAge(Director dir2) {
		if(super.getAge() == dir2.getAge()) {
			System.out.println(super.getName() + " " + dir2.getName() + " have the same age.");
		}else if(super.getAge() > dir2.getAge()) {
			System.out.println(super.getName() + " is older than " + dir2.getName() + ".");
		}else {
			System.out.println(dir2.getName() + " is older than " + super.getName() + ".");
		}
	}
/*
	public void setStudio(int price, int hours) {
		 st1.setPrice(price);
		 st1.setWorkingHours(hours);
	}
	
	public Studio getStudio() {
		return st1;
	}
	*/
	
	@Override
	public String toString() {
		return "Name: " + super.getName() + "/n" +
				"Age: " + super.getAge();
	}
}