package com.studio;

import java.util.*;

public class MarketPrice {
	private static List<Integer> marketPrices = new ArrayList<>();

	public static int getMarketPrice() {
		int storedV = 0;
		for(Integer price: marketPrices) {
			storedV = storedV + price;
		}
		return storedV/(marketPrices.size() + 1);
	}

	public static void setMarketPrices(int price) {
		marketPrices.add(price);
	}
}
