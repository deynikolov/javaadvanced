package com.studio;

public class MusicCompany {
	
	private Director director;
	private Studio studio;

	public MusicCompany(Director director, Studio studio) {
		this.director = director;
		this.studio = studio;
	}
	
	public void rentStudio(Client client, int hours) {
		if(hours < studio.getWorkingHours()) {
		studio.setWorkingHours(studio.getWorkingHours() - hours);
		studio.setTotal(studio.getPrice() * hours);
		System.out.println(client.getName() + " rent the studio for " + hours);
		}else {
			System.out.println(client.getName() + " cant rent the studio for " + hours);
		}
	}
	
	public String checkPrices(Studio st2) {
		if(studio.getPrice() > st2.getPrice()) {
			return studio.toString() + "\n" + "has better price than \n" + st2.toString();
		}else {
			return st2.toString() + "\n" + "has better price than \n" + studio.toString();
		}
	}

	public Director getDirector() {
		return director;
	}
}
