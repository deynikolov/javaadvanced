package com.store;

public class Customer {
	
	private int budget;

	public Customer(int budget) {
		this.setBudget(budget);
	}

	public int getBudget() {
		return budget;
	}

	public void setBudget(int budget) {
		this.budget = budget;
	}
	
	@Override
	public String toString() {
		return "The customer budget is " + this.budget + ".";
	}

}
