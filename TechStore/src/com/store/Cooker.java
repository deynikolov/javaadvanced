package com.store;

public class Cooker extends ElectricalDevice{
	
	private boolean isGas;

	public Cooker(Manufacturer man, int price, boolean isGas) {
		super(man, price);
		this.isGas = isGas;
	}
	
	@Override
	public int warrantyPeriod() {
		if(isGas) {
			return super.warrantyPeriod() + 12;
		}else {
			return super.warrantyPeriod();
		}
	}
	
	@Override
	public String toString() {
		return 	super.toString() + "\n" + 
					"Gas: " + this.isGas;	
	}
}
