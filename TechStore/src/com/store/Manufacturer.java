package com.store;

public class Manufacturer {

	private String name;
	private boolean extendWarranty;
	
	public Manufacturer(String name, boolean extendWarranty) {
		this.name = name;
		this.extendWarranty = extendWarranty;
	}
	
	public String getName() {
		return this.name;
	}

	public boolean isExtendWarranty() {
		return extendWarranty;
	}
	
	@Override
	public String toString() {
		return 	"Manufacturer: " + this.name + "\n" + 
				"Extends warranty: " + this.extendWarranty;
	}
	
}