package com.store;

public class WashingMachine extends ElectricalDevice{
	
	private int revPerMin;
	private boolean isDryingClothes;

	public WashingMachine(Manufacturer man, int price, int revPerMin, boolean isDryingClothes) {
		super(man, price);
		this.revPerMin = revPerMin;
		this.isDryingClothes = isDryingClothes;
	}
	
	@Override
	public int warrantyPeriod() {
		if(isDryingClothes) {
			return super.warrantyPeriod() + 6;
		}else {
			return super.warrantyPeriod();
		}
	}
	
	@Override
	public String toString() {
		return 	super.toString() + "\n" + 
				"Rev per minute: " + this.revPerMin + "\n" +
				"Drying clother: " + this.isDryingClothes;	
	}
	
}
