package com.store;

public class ElectricalDevice {
	private Manufacturer man;
	private static int minWarranty = 12;
	private int warranty, price;

	public ElectricalDevice(Manufacturer man, int price) {
		this.man = man;
		this.setPrice(price);
		warranty = minWarranty;
	}
	
	public int warrantyPeriod() {
		if(man.isExtendWarranty()) {
			warranty = warranty + 12;
		}
		return warranty;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return 	"Manufacturer: " + man.getName() + "\n" + 
				"Warranty: " + this.warranty + "\n" + 
				"Price: " + this.price;
	}
	
}
