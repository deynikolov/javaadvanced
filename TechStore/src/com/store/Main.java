package com.store;

public class Main {
	public static void main(String[] args) {
		ElectricalDevice cooker, gasCooker, wMachine, wMachineDry;
		Manufacturer bosch, samsung;
		Customer cust1, cust2;
		Store store;
		
		bosch = new Manufacturer("Bosch", false);
		samsung = new Manufacturer("Samsung", true);
		cooker = new Cooker(bosch, 60, false);
		gasCooker = new Cooker(samsung, 70, true);
		wMachine = new WashingMachine(bosch, 85, 700, false);
		wMachineDry = new WashingMachine(samsung, 140, 850, true);
		cust1 = new Customer(70);
		cust2 = new Customer(80);
		store = new Store("TechStore");
		store.addInStore(wMachineDry);
		store.addInStore(cooker);
		store.addInStore(gasCooker);
		store.addInStore(wMachine);
		
		store.sellProduct(gasCooker, cust1);
		store.sellProduct(wMachine, cust2);
		store.sellProduct(gasCooker, cust2);
	}

}