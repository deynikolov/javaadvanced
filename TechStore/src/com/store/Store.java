package com.store;

import java.util.*;

public class Store {

	private String name;
	private List<ElectricalDevice> inStore;
	
	public Store(String name) {
		this.name = name;
		this.inStore = new ArrayList<ElectricalDevice>();
	}
	
	public String getName() {
		return this.name;
	}
	
	public void addInStore(ElectricalDevice device) {
		inStore.add(device);
	}
		
		
	public void sellProduct(ElectricalDevice device, Customer customer) {
		if(!inStore.contains(device)) {
			System.out.println("The device you want to sell is not available." + "\n");
		}else if(customer.getBudget() < device.getPrice()) {
			System.out.println("You cant buy this device." + "\n");
		}else if(customer.getBudget() >= device.getPrice()) {
			if(inStore.contains(device)) {
				customer.setBudget(customer.getBudget() - device.getPrice());
				System.out.println(device.toString() + "\n" + "is sold." + "\n");
				inStore.remove(device);
			}
		}else {System.out.println("The device you want to sell is not available." + "\n");}
	}
	
	@Override
	public String toString() {
		return 	"Store name: " + this.name + "\n" + 
				"Devices in store: " + inStore;
	}
}
