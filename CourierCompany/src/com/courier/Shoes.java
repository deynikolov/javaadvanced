package com.courier;

public class Shoes extends Item{
	
	private boolean tryOn = false;
	private double shippingPrice;

	public Shoes(boolean tryOn) {
		this.tryOn = tryOn;
	}

	public boolean istryOn() {
		return tryOn;
	}
	
	public double getShippingPrice() {
		return shippingPrice;
	}
	
	@Override
	public void shippingPrice() {
		shippingPrice = 0;
		if(tryOn) {
			shippingPrice = super.totalWeight() * super.priceForKilogram * (3/100);
		}else {
			shippingPrice = super.totalWeight() * super.priceForKilogram;
		}
	}

	@Override
	public String toString() {
		return 	"Shipping price: " + shippingPrice + "\n" +
				"Try on: " + tryOn;
	}

}
