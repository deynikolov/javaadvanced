package com.courier;

import java.util.ArrayList;
import java.util.List;

public class CourierCompany {
	
	private List<Item> items;
	private double earnings;
	private double volume;

	public CourierCompany() {
		items = new ArrayList<Item>();
	}
	
	public void addElement(Box box ) {items.add(box.getItem());}
	
	public double getTotalEarnings() {
		earnings = 0;
		for(Item item: items) {
			earnings = earnings + item.getShippingPrice();
		}
		return earnings;
	}
	
	public void totalVolume() {
		volume = 0;
		for(Item item: items) {
			this.volume = volume + item.getVolume();
		}
	}
	
	public double getVolume() {return volume;}

	@Override
	public String toString() {
		return 	"Earnings: " + earnings + "\n" +
				"Total volume: " + volume;
	}
}
