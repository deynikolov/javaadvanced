package com.courier;

public class Element {
	
	private String name;
	private double amountOfMaterialInKilograms;
	private double volume;
	private Material material;

	public Element(String name, double amountOfMaterialInKilograms, double volume, Material material) {
		this.name = name;
		this.amountOfMaterialInKilograms = amountOfMaterialInKilograms;
		this.volume = volume;
		this.material = material;
	}

	public String getName() {
		return name;
	}
	
	public double getAmountOfMaterialInKilograms() {
		return amountOfMaterialInKilograms;
	}

	public boolean isFragile() {
		return material.isFragile();
	}
	
	public double getVolume() {
		return volume;
	}
	
	@Override
	public String toString() {
		return 	material.toString() + "\n" +
				"Amount of material used in kilograms: " + amountOfMaterialInKilograms;
	}

}
