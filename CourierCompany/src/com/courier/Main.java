/**
 * 
 */
package com.courier;

/**
 * @author ADMIN
 *
 */
public class Main {
	public static void main(String[] args) {
		Material wood = new Material("Wood", true);
		Material leather = new Material("leather", false);
		Element chairLeg = new Element("chairLeg", 0.300, 46, wood);
		Element chairLeg2 = new Element("chairLeg2", 0.300, 46, wood);
		Element chairLeg3 = new Element("chairLeg3", 0.300, 46, wood);
		Element chairLeg4 = new Element("chairLeg4", 0.300, 46, wood);
		Element chairTop = new Element("chairTop", 0.550, 60, wood);
		
		Item chair = new Furniture("Xbrand");
		chair.addElement(chairTop);
		chair.addElement(chairLeg);
		chair.addElement(chairLeg2);
		chair.addElement(chairLeg3);
		chair.addElement(chairLeg4);
		chair.calculateVolume();
		chair.shippingPrice();
		
		Element leftShoe = new Element("leftShoe", 0.360, 35, leather);
		Element rightShoe = new Element("rightShoe", 0.360, 35, leather);
		Item shoes = new Shoes(true);
		shoes.addElement(leftShoe);
		shoes.addElement(rightShoe);
		shoes.calculateVolume();
		shoes.shippingPrice();
		
		Box box1 = new Box(2, "325z4f53", "st. 'Krum' No:45", chair);
		Box box2 = new Box(1, "1234as51", "St. 'Ivan' No:21", shoes);
		CourierCompany fedex = new CourierCompany();
		fedex.addElement(box1);
		fedex.addElement(box2);
		System.out.println(fedex.getTotalEarnings());
		fedex.totalVolume();
		System.out.println(fedex.getVolume());
		System.out.println(fedex.toString());
		
	}

}
