package com.courier;

public interface Shipment {
	public static final double priceForKilogram = 1.4;
	public static final double maxVolume = 3000;
	public void shippingPrice();
}
