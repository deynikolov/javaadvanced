package com.courier;

import java.util.ArrayList;
import java.util.List;

abstract class Item implements Shipment {
	
	private List<Element> elements;
	private double volume;

	public Item() {
		elements = new ArrayList<Element>();
	}

	public void addElement(Element element) {elements.add(element);}
	
	public List<Element> getElements() {
		return elements;
	}
	
	public double totalWeight() {
		double weight = 0;
		for(Element element: elements) {
			weight = weight + element.getAmountOfMaterialInKilograms();
		}
		return weight;
	}
	
	public void calculateVolume() {
		volume = 0;
		for(Element element: elements) {
			volume = volume + element.getVolume();
		}
	}
	
	public double getVolume() {
		return volume;
	}
	
	public boolean isFragile() {
		boolean itemIsFragile = false;
		for(Element element: elements) {
			if(element.isFragile()) {itemIsFragile = true; break;}
		}
		return itemIsFragile;
	}
	
	@Override
	public abstract void shippingPrice();

	@Override
	public String toString() {
		return "The elements inside the item: " + elements;
	}

	protected abstract double getShippingPrice();
}
