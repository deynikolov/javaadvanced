package com.courier;

public class Material {
	
	private String name;
	private boolean isFragile;

	public Material(String name, boolean isFragile) {
		this.name = name;
		this.isFragile = isFragile;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isFragile() {
		return isFragile;
	}

	@Override
	public String toString() {
		return 	"The material is " + name + "\n" + 
				"Is fragile: " + isFragile;
	}
}
