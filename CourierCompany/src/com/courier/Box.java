package com.courier;

class Box {
	
	private Item item;
	private int size;
	private String signature;
	private String address;
	private double maxVolume;

	public Box(int size, String signature, String address, Item item) {
		if(!(1 <= size && size <=3)) {System.out.println("There is no such size of the box");}
		else {
			if(size == 1) {this.maxVolume = 500;}
			else if(size == 2) {this.maxVolume = 1000;}
			else {this.maxVolume = 3000;}
			this.item = item;
			this.size = size;
			this.signature = signature;
			this.address = address;
		}
	}

	public Item getItem() {
		return item;
	}

	public int getSize() {
		return size;
	}

	public String getSignature() {
		return signature;
	}

	public String getAddress() {
		return address;
	}

	public double getMaxVolume() {
		return maxVolume;
	}
	
}