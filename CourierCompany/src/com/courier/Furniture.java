package com.courier;

public class Furniture extends Item{
	
	private String brand;
	private double shippingPrice;
	
	public Furniture(String brand) {
		this.brand = brand;
	}
	
	public String getBrand() {
		return brand;
	}
	
	public double getShippingPrice() {
		return shippingPrice;
	}

	@Override
	public void shippingPrice() {
		shippingPrice = 0;
		if(super.isFragile() & super.getVolume() > super.maxVolume) {
			shippingPrice = super.totalWeight() * super.priceForKilogram * (1.5/100);
		}else {
			shippingPrice = super.totalWeight() * super.priceForKilogram;
		}
		 
	}

	@Override
	public String toString() {
		return 	"Brand: " + brand + "\n" +
				"Shipping price " + shippingPrice;
	}

}
