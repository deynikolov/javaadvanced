package com.employment;

public class Employee {
	
	private String name;
	private static int numberOfEmployees = 0;
	private int id;
	private int workingHoursForMonth;
	private double bonusSalary;
	private ContractType contract;

	public Employee(String name, double bonusSalary, ContractType contract) {
		this.name = name;
		numberOfEmployees++;
		this.id = numberOfEmployees;
		workingHoursForMonth = 0;
		this.bonusSalary = bonusSalary;
		this.contract = contract;
	}
	
	public double totalSalary() {
		return (contract.getSalary() + bonusSalary) * workingHoursForMonth;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}
	
	public void setBonusSalary(double bonusSalary) {
		this.bonusSalary = bonusSalary;
	}
	
	public double getBonusSalary() {
		return bonusSalary;
	}
	
	public String getTypeOfContract() {
		return contract.getTypeOfContract();
	}

	@Override
	public String toString() {
		return 	"ID: " + id + "\n" + 
				"Name: " + name + "\n" +
				"Contract type: " + contract.getTypeOfContract() + "\n" + 
				"Bonus salary: " + bonusSalary + "\n" +
				"Working hours in month: " + workingHoursForMonth;
	}
}
