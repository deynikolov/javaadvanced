package com.employment;

public class Main {

	public static void main(String[] args) {
		ContractType permanent = new ContractType("Permanent", 70);
		ContractType partTime = new ContractType("Part time", 50);
		ContractType trainee = new ContractType("Trainee", 35);
		Company ikea = new Company("IKEA", 3, permanent, partTime, trainee);
		ikea.hireEmployee("Ivan", 20, "Part time");
		ikea.listEmployees();
	}

}
