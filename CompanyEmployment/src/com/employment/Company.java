package com.employment;

import java.util.*;

public class Company {
	
	private String name;
	private int maxNumberOfEmployees;
	private ContractType permanent, partTime, trainee;
	private List<Employee> employees;
	private double total;

	public Company(String name, int maxNumberOfEmployees, ContractType permanent, ContractType partTime, ContractType trainee) {
		this.name = name;
		this.maxNumberOfEmployees = maxNumberOfEmployees;
		this.permanent = permanent;
		this.partTime = partTime;
		this.trainee = trainee;
		employees = new ArrayList<Employee>();
		this.total = 0;
	}
	
	public String getName() {
		return name;
	}

	public void hireEmployee(String name, double bonusSalary, String contractType) {
		if(!(employees.size() == maxNumberOfEmployees)) {
			if(contractType.equals(permanent.getTypeOfContract())) {employees.add(new Employee(name, bonusSalary, permanent));}
			else if (contractType.equals(partTime.getTypeOfContract())) {employees.add(new Employee(name, bonusSalary, partTime));}
			else if (contractType.equals(trainee.getTypeOfContract())) {employees.add(new Employee(name, bonusSalary, trainee));}
			else {System.out.println("There is no such contract type!");}
		}else {System.out.println("You cant hire more employees!");}
	}
	
	public void fireEmployee(int id) {
		for(Employee employee: employees) {
			if(employee.getId() == id) {employees.remove(employee);}
		}
	}
	
	public void listEmployees() {
		for(Employee employee: employees) {System.out.println(employee.toString());}
	}
	
	public double sumOfSalaries() {
		for(Employee employee: employees) {total = total + employee.totalSalary();}
		return total;
	}
	
	
	public void singleEmployeeBonusSalaryIncrease(int id, double percentage) {
		for(Employee employee: employees) {if(employee.getId() == id) {employee.setBonusSalary(employee.getBonusSalary() * percentage);}}
	}
	
	public void employeesBonusSalaryIncrease(double percentage) {
		for(Employee employee: employees) {employee.setBonusSalary(employee.getBonusSalary() * percentage);}
	}
	
	public double totalSalaryOfConcreteContractType(String contractType) {
		double totalForTypeOfContract = 0;
		for(Employee employee: employees) {
			if(contractType.equals(employee.getTypeOfContract())) {
			totalForTypeOfContract = totalForTypeOfContract + employee.totalSalary();
			}
		}	
		return totalForTypeOfContract;
	}
	
	@Override
	public String toString() {
		return 	"Name of the company " + name + "\n" +
				"Max number of employees " + maxNumberOfEmployees + "\n" +
				"Current employee number " + employees.size();
	}
	
}
