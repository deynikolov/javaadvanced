package com.employment;

public class ContractType {
	
	private String typeOfContract;
	private int salary;
	
	public ContractType(String typeOfContract, int salary) {
		this.setTypeOfContract(typeOfContract);
		this.setSalary(salary);
	}

	public String getTypeOfContract() {
		return typeOfContract;
	}

	public void setTypeOfContract(String typeOfContract) {
		this.typeOfContract = typeOfContract;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
}
