package interpreter;

import java.util.List;

import interfaces.Subtract;

public class Subtraction implements Subtract {

	@Override
	public int subtract(int a, int b) {
		return a - b;
	}
	@Override
	public List<Integer> subtractArrays(List<Integer> list, int subtractor) {
		throw new UnsupportedOperationException();
	}
}
