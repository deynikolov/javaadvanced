package interpreter;

import java.util.HashMap;
import java.util.Map;
import interfaces.Memory;

public class MemoryStorage implements Memory{
	private Map<String, Integer> storage;

	public MemoryStorage() {
		storage = new HashMap<String, Integer>();
	}

	@Override
	public void storeVariables(String name, int value) {
		storage.put(name, value);
	}

	@Override
	public boolean checkVariables(String name) {
		boolean bool = false;
		for (Map.Entry<String, Integer> entry : storage.entrySet()) {
			if (entry.getKey().equals(name)) {
				bool = true;
				break;
			}
		}
		if(bool == false) {System.out.println("Variable " + name + " is not initialized!");}
		return bool;
	}

	@Override
	public int getValue(String name) {
		int a = 0;
		for (Map.Entry<String, Integer> entry : storage.entrySet()) {
			if (entry.getKey().equals(name)) {
				a = entry.getValue();
			}
		}
		return a;
	}
}
