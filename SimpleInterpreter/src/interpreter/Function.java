package interpreter;

/*
 * The class represent the function logic and stores its name and calculated value
 * It does not support the functionality to override functions
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Function {
	
	private String name;
	private MemoryStorage internalMem;
	private String input;
	private Scanner sc;
	private String[] arrOfStr;
	private List<String> list;
	private int value;

	public Function(String name) {
		this.name = name;
		internalMem = new MemoryStorage();
		sc = new Scanner(System.in);
	}
	
	public void functionRead() {
		while(true) {
			System.out.print("~");
			input = sc.nextLine();
			arrOfStr = input.split("\\s+");
			list = new ArrayList<String>(Arrays.asList(arrOfStr));
			if(list.get(0).equals("int") && list.get(1).matches(".*[a-z].*") && 
					list.get(2).equals("=") && list.get(3).matches("\\d+") && list.get(4).equals(";")) {
				Integer num = Integer.parseInt(list.get(3));
				internalMem.storeVariables(list.get(1), num);
				System.out.println(internalMem.getValue(list.get(1)));
			}
			else if(list.get(0).equals("return") && (list.get(list.size()-1).equals(";"))) {
				for(int i = 1; i < list.size()-1; i++) {
					if(internalMem.checkVariables(list.get(i))) {value = value + internalMem.getValue(list.get(i));}
					else if(list.get(i).matches("\\d+")) {value = value + Integer.parseInt(list.get(i));}
					else {continue;}
				}
			}
			else if(list.get(0).equals("}")) {break;}
			else {System.out.println("Invalid operator!");}
		}
	}

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return 	"Name: " + name + "\n" +
				"Value: " + value;
	}
}
