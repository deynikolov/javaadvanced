package interpreter;

import java.util.List;

import interfaces.Add;

public class Addition implements Add {

	@Override
	public int addition(int a, int b) {
		return a + b;
	}

	@Override
	public List<Integer> listAddtition(List<Integer> list, int add) {
		throw new UnsupportedOperationException();
	}

}
