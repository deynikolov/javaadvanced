package interpreter;

/*
 * Assert make comparison and return message if message is passed,
 * if not it only check and throw error if needed
 */

public class Assert {

	public static void check(int comparedNum, int comparingNum, String comparator) {
		switch(comparator) {
		case "<":
			if(comparedNum < comparingNum) {return;}
			else {throw new AssertionError(); }
		case ">":
			if(comparedNum > comparingNum) {return;}
			else {throw new AssertionError(); }
		case "==":
			if(comparedNum == comparingNum) {return;}
			else {throw new AssertionError(); }
		}
	}

	public static void check(int comparedNum, int comparingNum, String comparator, String message) {
		switch(comparator) {
		case "<":
			if(comparedNum < comparingNum) {return;}
			else {throw new AssertionError(message); }
		case ">":
			if(comparedNum > comparingNum) {return;}
			else {throw new AssertionError(message); }
	
		case "==":
			if(comparedNum == comparingNum) {return;}
			else {throw new AssertionError(message); }
		}
	}
}
