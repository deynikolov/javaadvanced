package interpreter;

/*
 * Its a class that implements memory management interface which store what it is passed to its functions
 * Memory is interface that could be implemented from every class that is used like storage
 * */

import java.util.*;

import interfaces.Memory;

public class FunctionStorage implements Memory{
	
	private List<Function> functions;

	public FunctionStorage() {
		functions = new ArrayList<Function>();
	}

	public void storeVariables(Function function) {functions.add(function);}
	
	public boolean checkVariables(String name) {
		boolean bool = false;
		for(Function function: functions) {
			if(function.getName().equals(name)) {bool = true; break;}
		}
		return bool;
	}
	
	public int getValue(String name) {
		int value = 0;
		for(Function function: functions) {
			if(function.getName().equals(name)) {value = function.getValue(); break;}
		}
		return value;
	}

	public void storeVariables(String type, int value) {
		throw new UnsupportedOperationException();
	}
}
