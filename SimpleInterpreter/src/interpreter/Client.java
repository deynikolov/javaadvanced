package interpreter;

/*
 * The class represent a client side of client-server architecture, if you want to run the program as a server
 * and you want to control it from other remote location you can use that functionality
 * the class has a main method that start the client and driver method that provide abstraction and checked message
 */

import java.net.*; 
import java.io.*;

public class Client { 

	public static void driver() {
	  try {
		main(null);
	  } catch (IOException e) {
		System.out.println("Something went wrong with the client!");
		e.printStackTrace();
	  } 
	}
  
   public static void main(String[] args)throws IOException {  
	 String server = null; 
	 InetAddress addr =  InetAddress.getByName(server); 
	 System.out.println("addr = " + addr); 
	 Socket socket = new Socket(addr, Server.PORT);                       				//Socket s=new Socket();
	 // Guard everything in a try-finally to make                                       //s.connect(new  InetSocketAddress(host,port),timeout); 
	 // sure that the socket is closed: 
	 try { 
	      System.out.println("socket = " + socket); 
	      BufferedReader in = 
	          new BufferedReader( 
	            new InputStreamReader(socket.getInputStream())); 
	      BufferedReader sin =  new BufferedReader( 
	            new InputStreamReader(System.in)); 
		   
	     PrintWriter out =   new PrintWriter( 
	            new BufferedWriter(  new OutputStreamWriter( 
		           socket.getOutputStream())),true); 
	     for(int i = 0; i <10 ; i ++) { 
	         System.out.print("input a line [disconnect for finish] " + i + ":"); 
	         String s = sin.readLine(); 
	          if(s.length()==0) break; 
	         out.println(i + ": " +s); 
	         String str = in.readLine(); 
	         System.out.println(str); 
	     } 
	     out.println("END"); 
                 } 
	 finally { 
		   System.out.println("closing..."); 
		   socket.close(); 
	 } 
       } 
} 
