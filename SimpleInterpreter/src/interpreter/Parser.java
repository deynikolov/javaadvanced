package interpreter;

/*
 * Parser read the input and translate it to the format that interpreter needs to run properly
 * we have a second state machine because here we it is designed for extensions like if we need 
 * to set int in format "int a = 5;" its true and it will set int but if we want to make case where int can
 * be declared like "a = 6" it could be plugged
 */

import java.util.List;

public class Parser {
	
	private static String checked;
	
	public static String parse(List<String> list) {
		checked = "CONTINUE";
		if(list.size() == 5 && list.get(0).equals("int") && list.get(1).matches(".*[a-z].*") && 
				list.get(2).equals("=") && list.get(3).matches("\\d+") && list.get(4).equals(";")) {checked = "INTEGER";}
		else if(list.size() == 3 && list.get(0).equals("def") && list.get(1).matches(".*[a-z].*") && list.get(2).equals("{")) {checked = "LEFT_CURLY_BRACKET";}
		else if(list.size() == 3 && list.get(0).matches(".*[a-z].*") && list.get(1).equals("+") && list.get(2).matches(".*[a-z].*")) {checked = "PLUS";}
		else if(list.size() == 3 && list.get(0).matches(".*[a-z].*") && list.get(1).equals("-") && list.get(2).matches(".*[a-z].*")) {checked = "MINUS";}
		else if(list.get(0).equals("assert") && list.get(1).matches(".*[a-z].*") && 
				(list.get(2).equals(">") || list.get(2).equals("<") || list.get(2).equals("=") || list.get(2).equals("=<") || list.get(2).equals("=>"))) {checked = "ASSERT";}
		else if(list.get(0).equals("connect")) {checked = "CONNECT";}
		else if(list.get(0).equals("test")) {checked = "TEST";}
		else {
			System.out.println("Invalid input:");
			checked = "CONTINUE";
		}
		return checked;
	}
}
