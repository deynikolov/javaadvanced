package interpreter;

/*
 * Switch through different case scenarios, its a state machine where, switch case check different scenarios
 * it depend on memory classes that stores variables and functions.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import myJunit.TestMain;

class SimpleInterpreter {
	private MemoryStorage memory;
	private FunctionStorage functionMemory;
	private String checked;
	private Addition add;
	private Subtraction sub;
	private String[] arrOfStr;
	private List<String> list;
	
	public SimpleInterpreter(MemoryStorage memory,FunctionStorage functionMemory) {
		this.memory = memory;
		this.functionMemory = functionMemory;
		add = new Addition();
		sub = new Subtraction();
	}
	
	public void interpret(String input) {
		arrOfStr = input.split(" ");
		list = new ArrayList<String>(Arrays.asList(arrOfStr));
		checked = Parser.parse(list);
		
		switch(Keywords.valueOf(checked)) {
			case LEFT_CURLY_BRACKET:
				Function function = new Function(list.get(1));
				function.functionRead();
				functionMemory.storeVariables(function);
				break;
			case PLUS:	
				System.out.println(add.addition(memory.getValue(list.get(0)), memory.getValue(list.get(2))));
				break;
			case MINUS:
				System.out.println(sub.subtract(memory.getValue(list.get(0)), memory.getValue(list.get(2))));
				break;
			case INTEGER:
				memory.storeVariables(list.get(1), Integer.parseInt(list.get(3)));
				break;
			case TEST:
				TestMain.driver();
				break;
			case ASSERT:	
				int comparedNum, comparingNum;
				try{
					comparedNum = Integer.parseInt(list.get(1));
					}catch(NumberFormatException ex) {
						if(memory.checkVariables(list.get(1))) {comparedNum = memory.getValue(list.get(1));}
						else {System.out.println("Var type is not supported!");break;}}
				try{
					comparingNum = Integer.parseInt(list.get(3));
					}catch(NumberFormatException e) {
						if(memory.checkVariables(list.get(3))) {comparingNum = memory.getValue(list.get(3));}
						else {System.out.println("Var type is not supported!");break;}}	
				if(list.size() > 3 ) {Assert.check(comparedNum, comparingNum, list.get(3), list.get(4));}
				break;
			case CONNECT:
				Client.driver();
				break;
			case CONTINUE:
				break;
			default:
				assert false : "Missing enum value: " + checked;
				break;
		}
	}
}
