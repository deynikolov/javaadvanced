package interpreter;

/*
 * "supported keywords or key symbols are: ""int"", ""="", "";"", ""+"", ""-"", ""{"", ""return""
 * assume the given code lines will be well formatted but think about correct
 * implementation summary - the program should support int, +, - and creation of
 * functions that return result"
 */

enum Keywords {
	RETURN("return"),
	INTEGER("int"),
	TEST("test"),
	ASSERT("assert"),
	CONNECT("connect"),
	DISCONNECT("disconnect"),
	PLUS("+"),
	MINUS("-"),
	EQUALS("="),
	LEFT_CURLY_BRACKET("{"),
	RIGHT_CURLY_BRACKET("}"),
	COMMA(","),
	SEMICOLON(";"),
	CONTINUE("continue");


	private String value;
	Keywords(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value;
	}
}
