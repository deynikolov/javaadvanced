package interpreter;

/*
 * Server_port the server listen here for client that wants to connect
 * the class represent the server that listen for connection and if any connections are established
 * it calls the interpreter and start working with it, it has driver method that provide abstraction for the call of server and catch the checked exceptions
 */

import java.io.*;
import java.net.*;

public class Server {
	  public static final int PORT = 8080;
	  
	  public static void driver() {
		  try {
			main(null);
		} catch (IOException e) {
			System.out.println("Something went wrong with the server!");
			e.printStackTrace();
		} 
	  }
	  
	  public static void main(String[] args)
	      throws IOException {
		SimpleInterpreter si = new SimpleInterpreter(new MemoryStorage(),new FunctionStorage());
	    ServerSocket s = new ServerSocket(PORT);   
	    System.out.println("Started: " + s);
	    try {
	      // Blocks until a connection occurs:
	      Socket socket = s.accept();                                           
	      try {                                                                                //
	        System.out.println(
	          "Connection accepted: "+ socket);
	        BufferedReader in =
	          new BufferedReader(
	            new InputStreamReader(
	              socket.getInputStream()));
	        // Output is automatically flushed
	        // by PrintWriter:
	        PrintWriter out =
	          new PrintWriter(
	            new BufferedWriter(
	              new OutputStreamWriter(
	                socket.getOutputStream())),true);
	        while (true) {
	          String str = in.readLine();
	          if (str.equals("DISCONNECT")) break;
	          si.interpret(str);
	          System.out.println("Echoing: " + str);
	          out.println(str);
	        }
	      //close the two sockets
	      } finally {
	        System.out.println("closing...");
	        socket.close();
	      }
	    } finally {
	      s.close();
	    }
	  }
	}