package interpreter;

/*
 * Starting point of the program
 */


import java.util.Scanner;

public class Controller{
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		String input;
		Scanner sc = new Scanner(System.in);
		SimpleInterpreter si = new SimpleInterpreter(new MemoryStorage(),new FunctionStorage());
		
		System.out.println("The program started");
		while (true) {
			System.out.print(">");
			input = sc.nextLine();
			if(input.equals("DISCONECT")) break;
			si.interpret(input);
		}
		sc.close();
	}
}
