package interfaces;

public interface Memory {
	void storeVariables(String type, int value);
	boolean checkVariables(String name);
	int getValue(String name);
}
