package interfaces;

import java.util.List;

public interface Subtract {
	public int subtract(int a, int b);
	public List<Integer> subtractArrays(List<Integer> list, int subtractor);
}
