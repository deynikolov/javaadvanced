package myJunit;

import java.util.Scanner;

import customAnnotations.After;
import customAnnotations.Before;
import customAnnotations.Test;

class UnitTest {

    @Before
    void TestStarted (){
        System.out.println("Test Started");
    }

    @Test
    boolean TestTriangleExist() throws Exception {
        double a, b, c;

            Scanner sc = new Scanner(System.in);
            System.out.println("Input a, b, c: ");
            a = Double.parseDouble(sc.nextLine());
            b = Double.parseDouble(sc.nextLine());
            c = Double.parseDouble(sc.nextLine());
            if((a + b) > c && (b + c) > c && (c + a) > b) {
            return true;
            }else {
            return false;
            }
    }

    @After
    void TestFinished() {
        System.out.println("Test finished");
    }
}