package myJunit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import customAnnotations.After;
import customAnnotations.Before;
import customAnnotations.Test;

public class AnnotationLogic {
	private Class<?> cls = null;
	private Method[] methods;
	private int length, completed, failed;
	private String methodName;
	private Object obj;

	public AnnotationLogic() {

	}

	void testExecute(String clsName) {
		this.completed = 0;
		this.failed = 0;
		try {
			cls = Class.forName(clsName);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		methods = cls.getDeclaredMethods();
		length = methods.length;
		try {
			obj = cls.getDeclaredConstructor().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < length; i++) {
			Method method = methods[i];
			if (method.isAnnotationPresent(Before.class)) {
				try {
					method.invoke(obj);
				} catch (Throwable e) {
					System.out.println("Exception in @Before");
				}
			}
		}

		for (int i = 0; i < length; i++) {
			Method method = methods[i];
			methodName = method.toString();
			if (method.isAnnotationPresent(Test.class)) {
				try {
					method.invoke(obj);
					completed++;
					System.out.println("Test completed successfuly.");
					System.out.println();
				} catch (Exception e) {
					failed++;
					System.out.println("Test failed.");
					System.out.println();
				}
			}
		}
		for (int i = 0; i < length; i++) {
			Method method = methods[i];
			if (method.isAnnotationPresent(After.class)) {
				try {
					method.invoke(obj);
				} catch (Throwable e) {
					System.out.println("Exception in @After");
				}
			}
		}
	}
	
	public int getCompleted() {
		return this.completed;
	}
	
	void setCompleted() {
		this.completed = 0;
	}
	
	public int getFailed() {
		return this.failed;
	}
	
	void setFailed() {
		this.failed = 0;
	}
	
	public String getMethodName() {
		return methodName;
	}
}
