package myJunit;

import java.util.Scanner;

public class TestMain {
	
	public static void driver() {
		main(null);
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		AnnotationLogic testCls = new AnnotationLogic();
		Log log = new Log();
		int choice = 0;

		
		while (true) {
			System.out.println("This is Unit test program pick a choice to continue!");
			System.out.println("1.Exectute test class by providing its full name.");
			System.out.println("2.Show test log.");
			System.out.println("3.Which test failed most.");
			System.out.println("4.Which test completed most.");
			System.out.println("5.Exit program.");
			System.out.print(">");
			sc.hasNext();
			choice = Integer.parseInt(sc.nextLine());
			
			
			if(choice == 5) {System.out.println("Bye, bye!"); break;}
			else if(choice == 1) {
				System.out.println("Write class name:");
				System.out.print(">");
				String methodName = sc.nextLine();
				testCls.testExecute(methodName);
				log.setKey(methodName);
				if(testCls.getCompleted() == 1) {
					log.setValue(methodName, true);
					testCls.setCompleted();
				}else if(testCls.getFailed() == 1) {
					log.setValue(methodName, false);
					testCls.setFailed();
				}
			}else if (choice == 2) {
				log.readLog();
			}else if (choice == 3) {
				System.out.println(log.logMostFailed());
			}else if (choice == 4) {
				System.out.println(log.logMostCompleted());
			}
		}
		sc.close();
	}
}
