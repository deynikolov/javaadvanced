package MyJunit;

import CustomAnnotations.After;
import CustomAnnotations.Before;
import CustomAnnotations.Test;

import java.util.Scanner;

class UnitTest {

    @Before
    void TestStarted (){
        System.out.println("Test Started");
    }

    @Test
    boolean TestTriangleExist() throws Exception {
        double a, b, c;

            Scanner sc = new Scanner(System.in);
            System.out.println("Input a, b, c: ");
            a = Double.parseDouble(sc.nextLine());
            b = Double.parseDouble(sc.nextLine());
            c = Double.parseDouble(sc.nextLine());
            if((a + b) > c && (b + c) > c && (c + a) > b) {
            return true;
            }else {
            return false;
            }
    }

    @After
    void TestFinished() {
        System.out.println("Test finished");
    }
}