package MyJunit;

class Triangle {
    private double a;
    private double b;
    private double c;

    Triangle(double a, double b, double c) throws Exception {
        this.a = a;
        this.b = b;
        this.c = c;
        if(!triangleExist()) throw new Exception();
    }

    private boolean triangleExist() {
        if ((a + b) > c && (b + c) > c && (c + a) > b) {
            return true;
        }else{
            return false;
        }
    }
    
    private boolean pythagorean() {
    	return a*a + b*b == c*c;
    }
}
