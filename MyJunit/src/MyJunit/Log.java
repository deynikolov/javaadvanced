package MyJunit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class Log {
	private Map<String, List<Integer>> Log;
	private List<Integer> counter;
	private int failed = 0;
	private int completed = 0;

	public Log() {
		Log = new HashMap<String, List<Integer>>();
		counter = new ArrayList<Integer>();
		counter.add(failed);
		counter.add(completed);
	}

	void setKey(String methodName) {
	/*	counter = new ArrayList<Integer>();
		counter.add(failed);
		counter.add(completed);*/
		Log.put(methodName, counter);
	}

	void setValue(String methodName, boolean change) {
		for (Entry<String, List<Integer>> entry : Log.entrySet()) {
			String key = entry.getKey();
			List<Integer> value = entry.getValue();
			if (key.equals(methodName) && change) {
				value.set(0, (int)value.get(0)+1);
			}else if(key.equals(methodName) && !change) {
				value.set(1, (int)value.get(1)+1);
			}
		}
	}

	void readLog() {
		System.out.println("This is the Test Log!");
		System.out.println("In left it show the name of the method,");
		System.out.println("In right [completed, failed]");
		for (Entry<String, List<Integer>> entry : Log.entrySet()) {
			String key = entry.getKey();
			List<Integer> value = entry.getValue();
			System.out.println(key + "  " + value);
		}
	}

	String logMostCompleted() {
		int max = 0;
		String methodName = null;
		for (Entry<String, List<Integer>> entry : Log.entrySet()) {
			String key = entry.getKey();
			List<Integer> value = entry.getValue();
			if(value.get(0) > max) {
				max = value.get(0);
				methodName = key;
			}
		}
		return "Method" + " " + methodName + " " + "completed" + " " + max + " " + "times.";
	}

	String logMostFailed() {
		int max = 0;
		String methodName = null;
		for (Entry<String, List<Integer>> entry : Log.entrySet()) {
			String key = entry.getKey();
			List<Integer> value = entry.getValue();
			if(value.get(1) > max) {
				max = value.get(1);
				methodName = key;
			}
		}
		return "Method" + " " + methodName + " " + "failed" + " " + max + " " + "times.";
	}
}

